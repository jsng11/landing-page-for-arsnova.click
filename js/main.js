//Cookie Consent JS
$(document).ready(function(){
    setTimeout(function () {
        $("#cookieConsent").fadeIn(200);
    }, 4000);
    $("#closeCookieConsent, .cookieConsentOK").click(function() {
        $("#cookieConsent").fadeOut(200);
    });
});

//JS für die Image Slideshow
//Array der Bilder wird angelegt
const IMAGES = document.querySelectorAll(".slide-image");
//Hilfsvariable zum Ansprechen des aktiven Bildes
let currentImage = 0;
//Hilfsvariable zum Ansprechen des previous-Buttons
let btnPrev = document.getElementById("btn-prev");
//Hilfsvariable zum Ansprechen des next-Buttons
let btnNext = document.getElementById("btn-next");
//onclick Methode des prev-Buttons
btnPrev.onclick = function () {
    //löscht die Klasseneigenschaft "active" aus allen Elementen des Arrays
    IMAGES.forEach(it => it.classList.remove("active"));
    //Hilfsvariable wird minus gerechnet um das davorige Bild anzusprechen
    currentImage = (currentImage - 1);
    //damit keine Minuszahlen ausgegeben werden eine Behandlung des Ausnahmefalls
    if(currentImage <0) currentImage = IMAGES.length -1;
    //Element an der Stelle der Hilfsvariable bekommt die Klasse "active" übergeben und wird somit angezeigt
    IMAGES[currentImage].classList.add("active");
}
//onclick Methode des next-Buttons
btnNext.onclick = function () {
    //löscht die Klasseneigenschaft "active" aus allen Elementen des Arrays
    IMAGES.forEach(it => it.classList.remove("active"));
    //Hilfsvariable wird inkrementiert um das davorige Bild anzusprechen und modulo der Array-Länge damit das Zahlen-Intervall eingehalten wird
    currentImage = (currentImage + 1) % IMAGES.length;
    //Element an der Stelle der Hilfsvariable bekommt die Klasse "active" übergeben und wird somit angezeigt
    IMAGES[currentImage].classList.add("active");
}

//JS für das Anzeigen der Modals Impressum und Datenschutz
// Suche die beiden Modals nach der ID und speichere diese jeweils in eine Variable ab.
    let modalI = document.getElementById("modal-impressum");
    let modalD = document.getElementById("modal-datenschutz");

// Suche die beiden Buttons für die Modals nach der ID und speichere diese jeweils in eine Variable ab.
    let btnI = document.getElementById("btn-impressum");
    let btnD = document.getElementById("btn-datenschutz");

// Suche die beiden close-"Buttons", zum Schließen der Modals, nach der ID und speichere diese jeweils in eine Variable ab.
    let spanI = document.getElementById("close-impressum");
    let spanD = document.getElementById("close-datenschutz");

// Wenn einer der beiden Buttons gedrückt wird, soll das ensprechende Modal angezeigt werden.
    btnI.onclick = function () {
        modalI.style.display = "block";
    }
    btnD.onclick = function() {
        modalD.style.display = "block";
    }

// Wenn der close-Button des Modals gedrückt wird, soll sich das Modal schließen.
    spanI.onclick = function () {
        modalI.style.display = "none";
    }
    spanD.onclick = function() {
        modalD.style.display = "none";
    }

// Für den Fall das der Nutzer außerhalb des Modals klickt, soll sich das Modal ebenso schließen.
    window.onclick = function (event) {
        if (event.target == modalD) {
            modalD.style.display = "none";
        }
        if (event.target == modalI) {
            modalI.style.display = "none";
        }
    }

//JS für den Farbwechsel von arsnova.click
//Die einzelnen span-Elemente von der Klasse orange (ars,nova,.,click) sollen in ein Array gespeichert werden
const ORANGES = document.querySelectorAll(".orange");
    //Eine konstante Zeit wird festgelegt um das Intervall in kommenden Schritten anzusprechen
const TIME = 1000;
//Eine Hilfsvariable wird festgelegt um das Element im Array anzusprechen, das momentan vom Farbwechsel betroffen ist
let index = 0;
/*Die Methode runScript nimmt zu Beginn jedem Element die Klasse active (dient dem Farbwechsel),
danach wird das Element mit dem index der Hilfsvariable angesprochen und diesem active hinzugefügt
zuletzt wird index inkrementiert und modulo der Elementlänge gerechnet damit die Zahl im Intervall bleibt.
*/
const runScript = () => {
    ORANGES.forEach(it => it.classList.remove("active"));
    ORANGES[index].classList.add("active");

    index = (index + 1) % ORANGES.length;
}
//setInterval führt runScript wie bereits oben beschrieben aus in einem Intervall, welcher von der Variable TIME vorgegeben wird
setInterval(runScript, TIME);
